package fr.uge;

import java.net.URI;
import java.util.Map;

/**
 * A representation of a Http request with its method, URI, and other things like headers.
 *
 * @param method  the {@link HttpRequestReader.HttpMethod} used in the request
 * @param uri     the URI which contains the path and the query
 * @param version the Http version
 * @param fields  the fields of the header
 * @param body    the body of the request
 */
public record HttpRequest(HttpRequestReader.HttpMethod method, URI uri, String version, Map<String, String> fields,
                          String body) {
}
