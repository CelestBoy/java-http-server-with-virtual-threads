package fr.uge;

import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

/**
 * A reader than can parse a {@link HttpRequest} from a given {@link ByteBuffer} and a {@link SocketChannel} connexion.
 */
public class HttpRequestReader {

  public static final String SP = String.valueOf(' ');
  public static final String CR = String.valueOf('\r');
  public static final String LF = String.valueOf('\n');
  public static final String HT = String.valueOf('\t');
  public static final String DOUBLE_QUOTE = String.valueOf('"');
  public static final String CRLF = CR + LF;

  private final SocketChannel sc;
  private final ByteBuffer buffer;
  private final HttpStuffReader httpStuffReader;

  /**
   * creates a {@link HttpRequestReader} with a {@link SocketChannel} and an allocated {@link ByteBuffer}.
   * The {@link ByteBuffer} doesn't need to be big, it will grow if needed.
   *
   * @param sc the connexion with a client
   * @param buffer the given buffer to read the data from the client
   */
  public HttpRequestReader(SocketChannel sc, ByteBuffer buffer) {
    this.sc = sc;
    this.buffer = buffer;
    httpStuffReader = new HttpStuffReader(sc, buffer);
  }

  /**
   * An enum for the different Http methods.
   */
  public enum HttpMethod {
    GET, POST, PUT, DELETE;

    /**
     * Similar to {@link #valueOf(String)} but converts to uppercase before.
     *
     * @param method the desired method
     * @return the value of the enum representing the method.
     * @throws IllegalArgumentException if this enum type has no constant with the specified name
     */
    public static HttpMethod getMethodFromString(String method) {
      Objects.requireNonNull(method);
      return valueOf(method.toUpperCase(Locale.ROOT));
    }
  }

  /**
   * Reads inside the {@link ByteBuffer} and the {@link SocketChannel} if needed to parse a request.
   * The buffer must be in write mode (compact for example) and will remain in this mode after the call of this method.
   * At the end, the buffer may contain some data not related to the request so the user must keep it safe.
   *
   * @return the {@link HttpRequest} request parsed in the buffer.
   * @throws IOException if any IO problem
   */
  public HttpRequest parse() throws IOException {
    buffer.flip();

    try {
      var line = httpStuffReader.readLineCarriageReturnLineFeed();
      var values = line.split(SP, 3);
      var method = HttpMethod.getMethodFromString(values[0]);
      var uri = URI.create(values[1]);
      var version = values[2];

      var fields = new HashMap<String, String>();

      while (!line.isEmpty()) {
        line = httpStuffReader.readLineCarriageReturnLineFeed();
        if (line.isEmpty()) {
          break;
        }
        var key = line.split(":", 2)[0].trim();
        var value = line.split(":", 2)[1].trim();
        fields.merge(key, value, (oldValue, newValue) -> oldValue + "," + newValue);
      }

      int contentSize = Integer.parseInt(fields.getOrDefault("Content-Length", "0"));
      var body = httpStuffReader.readBytes(contentSize);

      return new HttpRequest(method, uri, version, fields, body);
    } finally {
      buffer.compact();
    }
  }
}
