package fr.uge;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A simple Http Server.
 * It uses blocking architecture with virtual threads.
 */
public class HttpServer {
  public static final int PORT = 80;
  private static final Logger logger = Logger.getLogger(HttpServer.class.getName());
  private final ExecutorService executorService = Executors.newVirtualThreadPerTaskExecutor();
  private final ServerSocketChannel serverSocketChannel;


  public HttpServer() throws IOException {
    serverSocketChannel = ServerSocketChannel.open();
    serverSocketChannel.bind(new InetSocketAddress(PORT));
  }

  /**
   * Start the server.
   *
   * @throws IOException if any IO problem
   */
  public void launch() throws IOException {

    logger.info("Server started on port " + PORT);
    while (!Thread.interrupted()) {
      var client = serverSocketChannel.accept();
      logger.info("Connection accepted from " + client.getRemoteAddress());

      executorService.execute(() -> {
        try {
          serve(client);
        } catch (IOException e) {
          logger.log(Level.INFO, "Connection terminated with client by IOException", e.getCause());
        } catch (InterruptedException e) {
          silentlyClose(client);
        } finally {
          silentlyClose(client);
        }
      });
    }
  }

  private void serve(SocketChannel sc) throws IOException, InterruptedException {
    System.out.println("serving with thread " + Thread.currentThread());
    var buffer = ByteBuffer.allocate(256); // initial capacity


    var httpRequestReader = new HttpRequestReader(sc, buffer);

    var request = httpRequestReader.parse();
    System.out.println(request);

    var content = request.uri().toString();

    var response = StandardCharsets.UTF_8.encode("""
            HTTP/1.0 200 OK
            Date: Fri, 31 Dec 1999 23:59:59 GMT
            Server: MyServer
            Content-Type: text/plain
            Content-Length: %d
            Expires: Sat, 01 Jan 2000 00:59:59 GMT
            Last-modified: Fri, 09 Aug 1996 14:21:40 GMT

            %s
            """.formatted(content.length(), content));
    sc.write(response);
    System.out.println("end of serve");
  }

  private void silentlyClose(Closeable sc) {
    if (sc != null) {
      try {
        sc.close();
      } catch (IOException e) {
        // Do nothing
      }
    }
  }

  public static void main(String[] args) throws IOException {
    var server = new HttpServer();
    server.launch();
  }

}
