package fr.uge;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * A utils class for reading Http stuff like lines finished by carriage return and line feed.
 */
public class HttpStuffReader {

  private final ByteBuffer buffer;
  private final SocketChannel sc;

  public HttpStuffReader(SocketChannel sc, ByteBuffer buffer) {
    this.buffer = buffer;
    this.sc = sc;
  }


  /**
   * Reads a line finished by a carriage return and line feed.
   * Reads inside the {@link ByteBuffer} and the {@link SocketChannel} if needed.
   *
   * @return the line read excluding the carriage return and line feed
   *
   * @throws IOException if any IO problem
   */
  public String readLineCarriageReturnLineFeed() throws IOException {
    var sb = new StringBuilder();
    char current = 0;
    char previous;
    buffer.flip();

    while (true) {
      while (buffer.hasRemaining()) {
        previous = current;
        current = (char) buffer.get();
        sb.append(current);
        if (previous == '\r' && current == '\n') {
          buffer.compact();
          sb.setLength(sb.length() - 2);
          return sb.toString();
        }
      }
      // I have nothing mode to read in the buffer but I still haven't found the end of the line
      // so I read again from the socket.
      buffer.clear();
      if (sc.read(buffer) == -1) {
        throw new RuntimeException("Cannot read from socket");
      }
      buffer.flip();
    }
  }


  /**
   * Reads nbBytes. Reads inside the {@link ByteBuffer} and the {@link SocketChannel} if needed.
   *
   * @param nbBytes the number of bytes that will be read
   * @return a {@link String} containing the nbBytes read
   * @throws IOException if any IO error occurs
   */
  public String readBytes(int nbBytes) throws IOException {
    int totalRead = 0;
    var sb = new StringBuilder();

    buffer.flip();

    while (totalRead != nbBytes) {
      if (!buffer.hasRemaining()) {
        if (sc.read(buffer) == -1) {
          throw new RuntimeException("Cannot read from socket");
        }
      }
      var currentChar = (char) buffer.get();
      sb.append(currentChar);
      totalRead++;
    }

    buffer.compact();
    return sb.toString();
  }
}
